import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Quote } from '../../shared/quote.model';

@Component({
  selector: 'app-quotes-details',
  templateUrl: './quotes-details.component.html',
  styleUrls: ['./quotes-details.component.css']
})
export class QuotesDetailsComponent implements OnInit {
  id!: string;
  quotes!: Quote[];
  url!: string;

  constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router) { }

  ngOnInit(): void {
    this.quotes = [];
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.getQuotes();
    });
  }

  getQuotes() {
    if (this.id === 'All' || !this.id) {
      this.url = 'https://plovo-57f89-default-rtdb.firebaseio.com/quotes.json';
    } else {
      this.url = `https://plovo-57f89-default-rtdb.firebaseio.com/quotes.json?orderBy="category"&equalTo="${this.id}"`;
    }
    this.http.get<{[id: string]: Quote}>(this.url)
      .pipe(map(result => {
        if (result === null) {
          return [];
        } else {
          return  Object.keys(result).map(id => {
            const quoteData = result[id];

            return new Quote(id, quoteData.author, quoteData.category, quoteData.text)
          })
        }
      }))
      .subscribe(quotes => {
        this.quotes = quotes;
      });
  }

  delete(id: string) {
    this.http.delete('https://plovo-57f89-default-rtdb.firebaseio.com/quotes/' + id + '.json').subscribe();
    this.http.get<{[id: string]: Quote}>('https://plovo-57f89-default-rtdb.firebaseio.com/quotes.json').subscribe();
    void this.router.navigate([`/quotes/${this.id}`]);
  }
}
