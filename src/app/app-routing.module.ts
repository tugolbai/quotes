import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewQuoteComponent } from './new-quote/new-quote.component';
import { QuotesComponent } from './quotes/quotes.component';
import { QuotesDetailsComponent } from './quotes/quotes-details/quotes-details.component';

const routes: Routes = [
  {path: '', component: QuotesComponent},
  {path: 'quotes/add', component: NewQuoteComponent},
  {path: 'quotes', component: QuotesComponent, children: [
      {path: ':id', component: QuotesDetailsComponent},
    ]},
  {path: 'quotes/:id/edit', component: NewQuoteComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
