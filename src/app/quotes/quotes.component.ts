import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-quotes',
  templateUrl: './quotes.component.html',
  styleUrls: ['./quotes.component.css']
})
export class QuotesComponent implements OnInit {
  categoryList = ['All', 'star-wars', 'famous-people', 'saying', 'humor', 'motivational'];

  constructor() { }

  ngOnInit(): void {
  }
}
