import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Quote } from '../shared/quote.model';

@Component({
  selector: 'app-new-quote',
  templateUrl: './new-quote.component.html',
  styleUrls: ['./new-quote.component.css']
})
export class NewQuoteComponent implements OnInit {
  categoryQ = 'star-wars';
  authorQ!: string;
  quoteText!: string;
  id!: string;
  headline!: string;

  constructor(public http: HttpClient, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      if (this.id) {
        this.headline = 'Edit Quote';
        this.http.get<Quote>('https://plovo-57f89-default-rtdb.firebaseio.com/quotes/' + this.id + '.json')
          .subscribe(quote => {
            this.authorQ = quote.author;
            this.quoteText = quote.text;
            this.categoryQ = quote.category;
          });
      } else {
        this.headline = 'Submit new Quote';
      }
    });
  }

  createQuote() {
    const author = this.authorQ;
    const category = this.categoryQ;
    const text = this.quoteText;
    const body = {author, category, text};

    if (this.id) {
      this.http.put('https://plovo-57f89-default-rtdb.firebaseio.com/quotes/' + this.id + '.json', body).subscribe();
    } else {
      this.http.post('https://plovo-57f89-default-rtdb.firebaseio.com/quotes.json', body).subscribe();
    }
    this.http.get<{[id: string]: Quote}>('https://plovo-57f89-default-rtdb.firebaseio.com/quotes.json').subscribe();
    void this.router.navigate([`/quotes/${this.categoryQ}`]);

  }
}
